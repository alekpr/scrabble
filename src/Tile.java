/**
 * Represents a tile in the Scrabble game.
 * @author Alek Ratzloff
 */
public class Tile {
	private char letter;
	private int value;
	
	/**
	 * Creates a new tile instance.
	 * @param letter the letter represented by this tile
	 * @param value the point value of this tile
	 */
	public Tile(char letter, int value)	
	{
		this.letter = letter;
		this.value = value;
	}
	
	/**
	 * @return the letter of this tile
	 */
	public char getLetter() {
		return this.letter;
	}
	
	/**
	 * @return the value in points that this tile gives
	 */
	public int getValue() {
		return this.value;
	}
}
