
import java.util.HashSet;
import java.util.ArrayList;

/**
 * An action that a player can play that removes N tiles from his/her hand and
 * then draws from the bag as his/her turn.
 *
 * @author Taylor King
 */
public class DumpTiles implements Action {
       private ArrayList<Tile> hand;
       private ArrayList<Character> letters;
       private Scrabble game;
    public DumpTiles(String letters, ArrayList<Tile> tiles, Scrabble game) {
        this.letters = new ArrayList<>();
       
        for(int i = 0; i < letters.length(); i++) {
            if(letters.charAt(i) != ',') {
                this.letters.add(letters.charAt(i));
            }
        }
        this.hand = tiles;
        this.game = game;
    }
    
    public boolean test() {
        HashSet<Character> available = new HashSet<>();
        for(int i = 0; i < this.hand.size(); i++) {
            available.add(this.hand.get(i).getLetter());
        }
        for(int i = 0; i < this.letters.size(); i++) {
            if(!available.contains(this.letters.get(i))) {
                return false;
            }
            available.remove(this.letters.get(i));
        }
        return true;
    }
    public int perform() {
      ArrayList<Tile> removing = matchTiles(this.letters);
      for(Tile t:removing) {
          this.hand.add(this.game.exchangeTile(t));
      }
      return 0;
      
    }
    public ArrayList<Tile> matchTiles(ArrayList<Character> list) {
        ArrayList<Tile> removing = new ArrayList<>();
        
        for(int i = 0; i < list.size(); i++) {
            for(int j = 0; j < this.hand.size(); j++) {
                if(list.get(i) == this.hand.get(j).getLetter()) {
                    removing.add(this.hand.get(j));
                    this.hand.remove(j);
                }
            }
        }
        return removing;
    }
}