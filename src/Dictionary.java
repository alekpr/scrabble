import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.HashSet;

/**
 * This is a class that represents a dictionary that can be used to check if a
 * word is valid. The dictionary loads a specified file, or "twl.txt" if none is
 * supplied.
 * @author Taylor King
 */
public class Dictionary {

	private File file;
	private HashSet<String> list;

	/**
	 * Creates a new dictionary object. This will load "twl.txt".
	 */
	public Dictionary() {
		this.list = new HashSet<>();
		try {
			this.file = new File("twl.txt");
			BufferedReader br1 = new BufferedReader(new FileReader(this.file));
			String line;
			while ((line = br1.readLine()) != null) {
				this.list.add(line);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e);
			System.exit(1);
		} catch (IOException e) {
			System.err.println(e);
			System.exit(2);
		}
	}

	/**
	 * Creates a new dictionary object. This will load the file, one word per line
	 * and add them to the hashset.
	 * @param f the specified file to load.
	 */
	public Dictionary(File f) {
		try {
			this.list = new HashSet<>();
			this.file = f;
			BufferedReader br1 = new BufferedReader(new FileReader(this.file));
			String line;
			while ((line = br1.readLine()) != null) {
				this.list.add(line);
			}
		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
		}
	}

	/**
	 * Determines if a word is in the dictionary.
	 * @param line the word to check
	 * @return whether the word is in the dictionary or not
	 */
	public boolean isWord(String line) {
		return this.list.contains(line);
	}
}
