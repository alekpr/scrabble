import java.util.Scanner;
import java.io.File;

/**
 * Main game class, contains the current board and tile bag. Handles player 
 * iteration and game progression.
 * @author Taylor King
 */
public class Scrabble {
  public Dictionary gameDictionary;
	Player[] players;
	Board mainBoard;
	public TileBag tBag;
	Scanner sc;	
	
	/**
	 * Creates a new instance of the game.
	 */
	public Scrabble() {
		tBag = new TileBag();
		mainBoard = new Board();
		gameDictionary = new Dictionary(new File("twl.txt"));
	}
	
	/**
	 * Grabs a tile from the local tile bag.
	 * @return a random tile from the tile bag.
	 */
	public Tile getTile() {
		return tBag.pullRandom();
	}
	public static void main(String[] args) {
		Scrabble game = new Scrabble();
		game.play();
	}
	
	/**
	 * @return gets the scanner object used by the game
	 */
	public Scanner getScanner() {
		return this.sc;
	}
	
	/**
	 * Starts the game, asks for the number of players to instantiate into the 
	 * array, creates the array and the players
	 */
	public void play() {
		sc = new Scanner(System.in);
		int players = 0;
//		while(players < 2 || players > 4) {
		System.out.println("How many Players? (2-4)");
		players = sc.nextInt();
//		}
		System.out.println("How many Computers (0-3)");
		int computers = sc.nextInt();
		this.players = new Player[players + computers];
		
		// sets up human players
		for(int i = 0; i < players; i++) {
			this.players[i] = new HumanPlayer(this);
		}
		
		// sets up computer players
		for(int i = players; i < computers + players; i++) {
			this.players[i] = new ComputerPlayer(this);
		}
		
		// give each player tiles
		for(int k = 0; k < this.players.length; k++) {
			for(int i = 0; i < 7; i++) {
				this.players[k].giveTile(tBag.pullRandom());
			}
		}
		
		mainBoard.printBoard(mainBoard.getBoard());

		int i = 0;
		// While there are still tiles in the bag, allow the game to continue
		while(tBag.size() > 0) {
			// Give this player their turn set it as the variable turn. 
			Action turn = this.players[i].takeTurn();
			// If the test() method returns true, perform the action
			if(turn.test()) {
				int score = turn.perform();
				// give the player the points they have earned.
				this.players[i].givePoints(score);
				
				// once the players turn is over, progress to the next player. A bad action is not handled yet, that still needs to be implemented.
				if(i == this.players.length - 1) {
					i = 0;
				} else {
					i++;
				}
			}
			mainBoard.printBoard(mainBoard.getBoard());
		}
	}
	
	/**
	 * The game board is a two-dimensional array
	 * @return the game board
         */
	public Tile exchangeTile(Tile t) {
            Tile r = this.tBag.pullRandom();
            this.tBag.addTile(t);
            return r;
        }
        public Slot[][] getBoard() { 
		return mainBoard.getBoard();	
	}
}	
