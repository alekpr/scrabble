/**
 * Represents a place on the board to put a tile.
 * @author Alek Ratzloff
 */
public class Slot {

	private Tile tile;
	private SlotType type;
	
	/**
	 * Creates a slot on the board
	 * @param type the point value modifier (e.g., double word, triple letter)
	 */
	public Slot(SlotType type) {
	this.type = type;
	}
	
	/**
	 * @return the modifier of the slot on the board
	 */
	public SlotType getType() {
		return this.type;
	}
	
	/**
	 * @return the tile occupying this slot. If no tile, then it returns null.
	 */
	public Tile getTile() {
		return this.tile;
	}
	
	/**
	 * Sets the tile in this slot.
	 * @param tile the tile to place in the slot
	 * @return the number of points gained by placing this tile
	 */
	public int setTile(Tile tile) {
		this.tile = tile;
		switch(this.type) {
			case DOUBLELETTER: // We can handle Double / Triple Letter on our own. 
			return 2 * this.tile.getValue(); // return the value of the tile * 2 if we are a DoubleLetterSlot
			case TRIPLELETTER:
			return 3 * this.tile.getValue(); // return tile * 3 if we are a triple Letter slot.
		}		
		return this.tile.getValue();
	}
}
