/**
 * Interface for later defining a players actions, overriden in the human player
 * class thus far, will be critical in implementing an AI.
 * @author Taylor King
 */
public interface Player {

	/**
	 * Performs some sort of action that is the player's turn. May be a pass
	 * action, a play tile action, or a dump tile action.
	 * @return the action that was taken by the player for this turn.
	 */
	public Action takeTurn();
	
	/**
	 * @return the name of the player
	 */
	public String getName();
	
	/**
	 * @param name the name to set the player to
	 */
	public void setName(String name);
	
	/**
	 * @return the number of points that the player has
	 */
	public int getPoints();
	
	/**
	 * Gives a player a certain amount of points.
	 * @param points the number of points to give to this player.
	 */
	public void givePoints(int points);
	
	/**
	 * Checks to see if a player has a letter in his/her hand.
	 * @param t the letter to check
	 * @return whether the player has a tile in his/her hand
	 */
	public boolean hasInHand(char t);
	
	/**
	 * Removes a tile from the player's hand
	 * @param t the letter to remove
	 * @return the tile from the player's hand
	 */
	public Tile takeFromHand(char t);
	
	/**
	 * Puts a tile in the player's hand
	 * @param t 
	 */
	public void giveTile(Tile t);
}
