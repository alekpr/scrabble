
import java.util.Scanner;
import java.util.ArrayList;

/**
 * An implementation for a human player from the Player interface.
 *
 * @author Taylor King
 */
public class HumanPlayer implements Player {

	private Scrabble game;
	private int score;
	private String name;
	private ArrayList<Tile> hand;

	/**
	 * Creates a new HumanPlayer object.
	 *
	 * @param caller the game of scrabble handling this player object
	 */
	public HumanPlayer(Scrabble caller) {
		this.hand = new ArrayList<>();
		this.score = 0;
		this.game = caller;
		Scanner rc = new Scanner(System.in);
		System.out.println("Enter a Player Name: ");
		this.name = rc.next();
	}

	/**
	 * Gives a specific tile to a player to hold in their hand
	 *
	 * @param t the tile to give
	 */
	public void giveTile(Tile t) {
		this.hand.add(t);
	}

	/**
	 * Sets the name of the player
	 *
	 * @param name the name to set the player to
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name of the player
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the player's score
	 */
	public int getPoints() {
		return this.score;
	}

	/**
	 * @return the number of tiles a player has in their hand
	 */
	public int numTiles() {
		return this.hand.size();
	}

	/**
	 * Issues this player a given number of points
	 *
	 * @param points the number of points to add to the player's score
	 */
	public void givePoints(int points) {
		this.score += points;
	}

	/**
	 * Takes the player's turn. Prints out the board, prompts the player for an
	 * action to perform, and checks validity.
	 *
	 * @return the action associated with the player's chosen actions
	 */
	public Action takeTurn() {

		// print out the hand
		System.out.print("Hand: ");
		for (int i = 0; i < this.hand.size(); i++) {
			System.out.print(this.hand.get(i).getLetter() + " ");
		}

		System.out.print("\nValue: ");
		for (int i = 0; i < this.hand.size(); i++) {
			System.out.print(this.hand.get(i).getValue() + " ");
		}

		System.out.println("\nIt's your turn " + this.name + ". What would you like to do? ");
		System.out.println("1. Play a Word. ");
		System.out.println("2. Test a Word. ");
		System.out.println("3. Exchange tiles. ");
		System.out.println("4. Pass");

		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt(); //Prompt the player for their move.

		while (true) {
			if (choice == 1) {
				boolean ud = false;
				while (true) {
					System.out.println("Enter the word you wish to play: ");
					String word = sc.next();
					System.out.println("Horizontal / Vertical (h/v)");
					String sw = sc.next();
					System.out.println("Enter the coordinate of the first letter in the form (L#)");
					String co = sc.next();

					switch (sw) { // switch the issue of horizontal / vertical to the boolean expected. 
						case "v":
							ud = true;
							break;
						case "h":
							ud = false;
							break;
						default: // Something needs to be put here to handle invalid input.
							break;
					}
					PlayWordAction act
							= new PlayWordAction(word, Character.getNumericValue(co.charAt(1)) - 1, co.charAt(0), this.hand, ud, this.game);
					return act;
				}
			} else if (choice == 2) { // Tests a word
				boolean ud = false;
				System.out.println("Enter the word you wish to play: ");
				String word = sc.next();
				System.out.println("Horizontal / Vertical (h/v)");
				String sw = sc.next();
				System.out.println("Enter the coordinate of the first letter in the form (L#)");
				String co = sc.next();

				switch (sw) {
					case "h":
						ud = true;
						break;
					case "v":
						ud = false;
						break;
				}
				PlayWordAction act
						= new PlayWordAction(word, Character.getNumericValue(co.charAt(1)) - 1, co.charAt(0), this.hand, ud, this.game);
				if (act.test()) {
					System.out.println("This is a valid word.");
				} else {
					System.out.println("This is NOT a valid word.");
				}
			} else if(choice == 3) { // Dump tiles action
				System.out.println("Enter the tiles you wish to remove, separated by commas:");
				String tiles = sc.next();
				return new DumpTiles(tiles, hand, this.game);
			} else if (choice == 4) { // Return a pass if the player chooses
				return new Pass();
			} else {
				choice = sc.nextInt();
			}
		}
	}

	/**
	 * Checks to see if a player has a given tile
	 *
	 * @param t the character of the tile to check
	 * @return whether or not the player has a tile of the character of the
	 * parameter in their hand
	 */
	public boolean hasInHand(char t) {
		for (int i = 0; i < this.hand.size(); i++) {
			if (this.hand.get(i).getLetter() == t) {
				return true;
			}
		}
		return false;
	}

	/**
	 * If the player has a Tile of the letter passed in their hand, it will be
	 * returned by this method.
	 *
	 * @param t the tile to grab
	 * @return the tile that is being taken out of the hand
	 */
	public Tile takeFromHand(char t) {
		for (int i = 0; i < this.hand.size(); i++) {
			if (this.hand.get(i).getLetter() == t) {
				Tile hold = this.hand.get(i);
				this.hand.remove(i);
				giveTile(this.game.getTile());
				return hold;
			}
		}
		return null;
	}
}
