/**
 * The Pass action that a player may use.
 * @author Taylor King
 */
public class Pass implements Action {
	public Pass() {}
	/**
	 * A Pass is a always a legal move.
	 * @return true, always
	 */
	public boolean test() {
		return true;
	}
	
	/**
	 * A player gets no points from making a pass.
	 * @return 0, always
	 */
	public int perform() {
		return 0;
	}
}
