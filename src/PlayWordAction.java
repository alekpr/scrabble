import java.util.ArrayList;

public class PlayWordAction implements Action {
	Scrabble game;
	String word;
	ArrayList<Tile> tiles;
	int col;
	int row;
	boolean up;
	Slot[][] board;

	public PlayWordAction(String word, int x, char y, ArrayList<Tile> tiles, boolean up, Scrabble game)	{
		// The word the player is hoping to make.
		this.word = word.toLowerCase();
		// The col coordinate of the first letter of the word
		this.col = x;
		// the row coordinate of the first letter of the word. Since we express this coordinate as an upper case letter, make sure it is lowercase, and subtract the letter a from it to give the actual coordinate on the board. Character.compare() returns the difference in two letters as an integer.
		this.row = Character.toLowerCase(y) - 'a';
		// The tiles available to construct the word ( pass the players entire hand the necessary tiles will be deducted placed on the board and replaced.
		this.tiles = tiles;
		// A reference to the current game, allows this class to access the board.
		this.game = game; 
		// call the game and get the board. 
		this.board = game.getBoard();
		// a boolean, whether or not the word is going to be played vertically or horizontally.
		this.up = up;  
	}
	
	//test the move and verify that it is playable. Return true if the test passes false if it does not.
	public boolean test() { 
    if(!this.game.gameDictionary.isWord(this.word))
      return false;
		// this boolean is to ensure that the word is either connected somehow to the first puzzle or the [7][7] center coordinate is used by the word.
		boolean connected = false; 
		
		//make a copy of the hand, do not actually take the tiles from the player as this is just a test.
		ArrayList<Tile> handCopy = copy(this.tiles);
		
		for(int i = 0; i < word.length(); i++) {
			// run this portion if the word is to be played horizontally.
			if(!up) { 
				if(row > 14 || col + i > 14) {
					return false; // If the word is ever longer than the edge of the board, the test fails.
					}
				if(row == 7 && col + i == 7) {
					// if the word passes through this coordinate, the test to see if the word is connected 
					// to the rest of the puzzle is not necessary because this must be the first play of the game
					connected = true; 
				}
				// another test to verify if the word is longer than the board.
				if(col + i > 14) {
					return false;
				}
				if(board[row][col + i].getTile() != null) { 
					// if the spot on the board is occupied, verify that the letter matches the one that we are trying to use.
					if(board[row][col + i].getTile().getLetter() != word.charAt(i)) {
						return false;
					}
					connected = true;
				} else { 
					// this will be run if the spot is unoccupied on the board, verifies that the player has the needed tile in their hand.
					for(int j = 0; j < handCopy.size(); j++) {
						if(Character.toLowerCase(handCopy.get(j).getLetter()) == word.charAt(i)) {
							handCopy.remove(i); // once we find the tile delete it so it cannot be used again.
							break;
						} 
					}
				}
			} else {
				if(row + i > 14 || col > 14) { //Once again verify that the word does not extend pass the edge of the board. 
					return false;
				}
        if(row + i == 7 && col == 7) {
          connected = true; // goes through the middle
        }
				Tile boardTile = board[row + i][col].getTile();
				if(boardTile != null) { // if the spot on the board already has a tile in it: 
					if(boardTile.getLetter() != word.charAt(i)) {
						return false; //if the tile does not match the letter being used, the move is invalid.
					} 
					connected = true; //if it does, the word is connected to the rest of the puzzle.
				} else {
					for(int j = 0 ; j < handCopy.size(); j++) {
						if(j == handCopy.size()) { // same as the tile remove function above.
							return false;
						}
						if(handCopy.get(j).getLetter() == word.charAt(i)) {
							handCopy.remove(i);
							break;
						}
					}
				}
			}
		}
		// if it's connected, we're in the clear.
		// if it's not, it'll return false. win-win.
		return connected; //if everything succeeds returns true 
	}
	
	public int perform() {
		int points = 0; 
		boolean doubleWord = false, tripleWord = false;		
		if(test()) {
			for(int i = 0; i < word.length();i++) {
				if(!up) {
					switch(board[row][col + i].getType()) { // if a player is playing on a special slot, be sure to give them the extra points for placing a tile in that slot. 
						case DOUBLEWORD:
							doubleWord = true;
							break;
						case TRIPLEWORD:
							tripleWord = true;
							break;
					}
					if(board[row][col + i].getTile() != null) {
						if(Character.toLowerCase(board[row][col + i].getTile().getLetter()) != word.charAt(i)) { //return a -1 in the case of a catastrophic error. This should never happen because test() should always be run first. 
							return -1;
						}
						points += board[row][col + i].getTile().getValue();	//add the value of the tile already on the board to the points for this played word
					}
					else {
						for(int j = 0; j < tiles.size(); j++) {
							if(Character.toLowerCase(tiles.get(j).getLetter()) == word.charAt(i)) {
								points+=board[row][col + i].setTile(tiles.get(j)); //Add the points to the turn by placing the tile in the slot and getting back the appropriate point value. Slot.setTile() returns 2* the tiles value for a DoubleLetter or 3* for a tripleLetter
								tiles.remove(j); //Take the tile out of the hand
								tiles.add(this.game.getTile());  //Get another tile from the bag.
								break;
							}
						}
					}
				} else { //Same as above just used for a vertical word instead of a horizontal one.
					switch(board[row + i][col].getType()) {
						case DOUBLEWORD:
							doubleWord = true;
							break;
						case TRIPLEWORD:
							tripleWord = true;
							break;
					} if(board[row + i][col].getTile() != null) {
						if(Character.toLowerCase(board[row + i][col].getTile().getLetter()) != word.charAt(i)) {
							return -1;
						}
						points+=board[row + i][col].getTile().getValue();
					} else {
						for(int j = 0; j < tiles.size(); j++) {
							if(Character.toLowerCase(tiles.get(j).getLetter()) == word.charAt(i)) {
								points+=board[row + i][col].setTile(tiles.get(j));
								tiles.remove(j);
								tiles.add(this.game.getTile());
							}
						}
					}	
				}
			}
			if(doubleWord) {
				points = points * 2;
			}
			if(tripleWord) {
				points = points * 3;
			}
		return points;
		}	else { // if the boolean is passed null, something is wrong.
			return -1; // this should never be encountered, perhaps we should write an exception class. 
		}	
	}
	public ArrayList<Tile> copy(ArrayList<Tile> toCopy) { // used to make a copy of the hand for the test method to remove tiles from the test hand.
		ArrayList<Tile> copy = new ArrayList<Tile>();
		for(int i = 0; i < toCopy.size(); i++) {
			copy.add(new Tile(toCopy.get(i).getLetter(),toCopy.get(i).getValue()));
		}
		return copy;
	}
}
