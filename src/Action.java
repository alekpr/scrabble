
/**
 * Each action a player can make will implement this interface
 * @author Taylor King
 */
public interface Action {
	/**
	 * This method performs the action and returns and int value of the points the
	 * player receives. Call the test method before this method.
	 * @return the number of points that the player receives. Failure is usually
	 * denoted by a -1 value.
	 */
	public int perform();
	/**
	 * Ensures that the move is a legal move.
	 * @return 
	 */
	public boolean test(); // 
}
