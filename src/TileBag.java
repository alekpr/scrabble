import java.util.Random;
import java.util.ArrayList;

/**
 * An array of items that is used to be randomly pulled from. Sort of like a set
 * but not quite.
 * @author Alek Ratzloff
 */
public class TileBag {
	private ArrayList<Tile> bag;

	public TileBag() {
		this.bag = new ArrayList<>();
		addTile('A',1,9);
		addTile('B',3,2);
		addTile('C',3,2);
		addTile('D',2,4);
		addTile('E',1,12);
		addTile('F',4,2);
		addTile('G',2,3);
		addTile('H',4,2);
		addTile('I',1,9);
		addTile('J',8,1);
		addTile('K',5,1);
		addTile('L',4,1);
		addTile('M',3,2);
		addTile('N',1,6);
		addTile('O',1,8);
		addTile('P',3,2);
		addTile('Q',10,1);
		addTile('R',1,6);
		addTile('S',1,4);
		addTile('T',1,6);
		addTile('U',1,4);
		addTile('V',4,2);
		addTile('W',4,2);
		addTile('X',8,1);
		addTile('Y',4,2);
		addTile('Z',10,1);			
}			
	
	public int size() {
		return this.bag.size();
	}
	public Tile pullRandom() {
		Random randomizer = new Random();
		int index = randomizer.nextInt(size());
		Tile hold = bag.get(index);
		bag.remove(index);
		return hold;	
	}
        public void addTile(Tile t) {
            this.bag.add(t);
        }
	public final void addTile(char letter, int value, int occurances) {
		for(int i = 0; i < occurances;i++) {
		this.bag.add(new Tile(letter,value));
		}
	}
}
