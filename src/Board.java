/**
 * This class defines the board for the game. When initialized it creates all of
 * the slots on the board with their respective point modifications enumerated
 * in the SlotType class. I (DoubleLetter, DoubleWord, etc)
 * 
 * Contains 2 Methods, getBoard() which returns a Slot[][] array and 
 * printBoard(Slot[][] board) which will print the board when passed the board 
 * as an argument.
 * 
 * @author Taylor King
 */
public class Board {
	private Slot[][] board;
	
	/**
	 * Creates a new instance of the game board. Adds all of the slots with
	 * appropriate point bonuses.
	 */
	public Board() {
		this.board = new Slot[15][15];
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				if((i == 14 || i == 7 || i == 0) && (j == 14 || j == 7 || j == 0) && !(j == 7 && i == 7)) {
					board[i][j] = new Slot(SlotType.TRIPLEWORD);
				} else if((i == j || 14 - i == j) && !(i == 5 || i == 9)  && (j == 5 || j == 9)) {
					board[i][j] = new Slot(SlotType.DOUBLEWORD);
				}	else if ((i == 1 || i == 5 || i == 9 || i == 13) && (j == 1 || j == 5 || j == 9 || j == 13)) {
					board[i][j] = new Slot(SlotType.TRIPLELETTER);
				}
				else if((i == 0 || i == 3 || i == 11 || i == 14) && (j == 0 || j == 3 || j == 11 || j == 14)) {
					board[i][j] = new Slot(SlotType.DOUBLELETTER);
				} else {
					board[i][j] = new Slot(SlotType.NORMAL);
				}
			}
		}
			board[2][6] = new Slot(SlotType.DOUBLELETTER);
			board[3][7] = new Slot(SlotType.DOUBLELETTER);
			board[2][8] = new Slot(SlotType.DOUBLELETTER);
			board[6][2] = new Slot(SlotType.DOUBLELETTER);
			board[7][3] = new Slot(SlotType.DOUBLELETTER);
			board[8][2] = new Slot(SlotType.DOUBLELETTER);

			board[12][6] = new Slot(SlotType.DOUBLELETTER);
			board[11][7] = new Slot(SlotType.DOUBLELETTER);
			board[12][8] = new Slot(SlotType.DOUBLELETTER);
			board[6][12] = new Slot(SlotType.DOUBLELETTER);
			board[7][11] = new Slot(SlotType.DOUBLELETTER);
			board[8][12] = new Slot(SlotType.DOUBLELETTER);

			board[6][6] = new Slot(SlotType.DOUBLELETTER);
			board[6][8] = new Slot(SlotType.DOUBLELETTER);
			board[8][6] = new Slot(SlotType.DOUBLELETTER);
			board[8][8] = new Slot(SlotType.DOUBLELETTER);
		}
	
		/**
		 * @return the representation of the board as a two-dimensional array of 
		 * slots
		 */
		public Slot[][] getBoard() {
			return this.board;
		}
					
		public void printBoard(Slot[][] board) {
			String header = "  " + "1" + " " + " " + "2" + " " + " " + "3" + " " + " "
					+ "4" + " " + " " + "5" + " " + " " + "6" + " " + " " +  "7" + " " 
					+ " " + "8" + " " + " " + "9" + " " + "10" + " " + " " + "11" + " " 
					+ " " + "12" + " " + " " + "13" + " " + " " + "14";
			// This labels the board kind of like a chess board, giving a player a 
			// notion of where they are going to place the tiles on the board. 
			System.out.println(header);
			// We start with the Letter A in the Top left corner, and continually 
			// increment along the left side. 
			char head = 'A'; 
				for(int i = 0; i < board.length; i++) {
					System.out.print(head);
					head++;

					for(int j = 0; j < board[i].length; j++) {
						System.out.print("[");
						switch(board[i][j].getType()) {
							case DOUBLELETTER:
								if(board[i][j].getTile() == null) {
									System.out.print("-");
								break;
								}
								System.out.print(board[i][j].getTile().getLetter());	
								break;
								case DOUBLEWORD:
								if(board[i][j].getTile() == null) {
									System.out.print("*");
									break;
									}
								System.out.print(board[i][j].getTile().getLetter());
								break;
								case TRIPLEWORD:
									if(board[i][j].getTile() == null) {
									System.out.print(".");
									break;
									}
									System.out.print(board[i][j].getTile().getLetter());
									break;
								case TRIPLELETTER:
									if(board[i][j].getTile() == null) {
									System.out.print("#");
									break;
									}
									System.out.print(board[i][j].getTile().getLetter());
								default:
									if(board[i][j].getTile() == null) {
										System.out.print(" ");
										break;
									} System.out.print(board[i][j].getTile().getLetter());
										break;
							}
						System.out.print("]");
						}	
					System.out.println();
					}
				}
			}
