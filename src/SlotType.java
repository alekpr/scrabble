/**
 * Adds a representation of the type of slots in which to place tiles on the 
 * board.
 * @author Alek Ratzloff
 */
public enum SlotType{
	NORMAL,
	DOUBLELETTER,
	DOUBLEWORD,
	TRIPLELETTER,
	TRIPLEWORD,
}
